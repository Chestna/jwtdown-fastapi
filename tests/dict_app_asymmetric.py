from fastapi import FastAPI, Depends
from fastapi.testclient import TestClient
from jwtdown_fastapi import authentication
import os
from jose.constants import ALGORITHMS
import pathlib


class DictionaryAuthenticator(authentication.Authenticator):
    get_exp_called = False
    exp_account = None

    async def get_account_data(
        self,
        username: str,
        accounts,
    ):
        return accounts.get_account(username)

    def get_account_getter(
        self,
        accounts: "Accounts" = Depends(),
    ):
        return accounts

    def get_hashed_password(
        self,
        account_data,
    ):
        return account_data["hashed_password"]

    def get_exp(self, proposed, account):
        self.get_exp_called = True
        self.exp_account = account
        return super().get_exp(proposed, account)

    def reset(self):
        self.get_exp_called = False
        self.exp_account = None


# Get key pair paths
certs_directory = os.path.abspath(
    os.path.join(pathlib.Path(__file__).parent.parent.resolve(), "tests/certs")
)
private_key_path = os.path.join(certs_directory, "RS256.key")
public_key_path = os.path.join(certs_directory, "RS256.key.pub")

# Read key pair files
private_key_file = open(private_key_path, "r")
private_key = "".join(private_key_file.readlines())

public_key_file = open(public_key_path, "r")
PUBLIC_KEY = "".join(public_key_file.readlines())

dict_auth = DictionaryAuthenticator(
    private_key,
    algorithm=ALGORITHMS.RS256,
    public_key=PUBLIC_KEY,
)

private_key_file.close()
public_key_file.close()


class Accounts:
    def get_account(self, username):
        return {
            "email": username,
            "age": 30,
            "hashed_password": dict_auth.hash_password("password"),
            "hashed_password_foo": dict_auth.hash_password("password"),
            "foo_password_hashed": dict_auth.hash_password("password"),
            "password": dict_auth.hash_password("password"),
        }


dict_app = FastAPI()
dict_app.include_router(dict_auth.router)


@dict_app.get("/protected")
async def get_protected(
    account: dict = Depends(dict_auth.get_current_account_data),
):
    return account


@dict_app.get("/not_protected")
async def get_protected(
    account: dict = Depends(dict_auth.try_get_current_account_data),
):
    return account


dict_client = TestClient(dict_app)
